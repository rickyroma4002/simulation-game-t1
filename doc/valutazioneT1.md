## GRUPPO 1 
Belletti - Halady - Franciosi - Fontana - Romano

"VIRUS"


# Valutazione Presentazione e Prodotto (4 su 5, Peer 3,9)

Il prodotto si distingue per alcune peculiarità, come ad esempio il tema, ma anche la scelta
tecnica di usare JavaFX, e il fatto di presentare una certa interattività, chelo rende più 
un gioco che una simulazione. Si tratta indubbiamente di una scelta "furba", che ha permesso di 
limitare la quantità di codice necessario permettendo al gruppo di approfondire aspetti operativi. 

La presentazione è stata complessivamente efficace e a tratti coinvolgente. 


# Valutazione ultima consegna (4 su 5)

L'UML è stato aggiornato ex-post, tuttavia è pregevole
Manca qualche documento di pianificazione.
Nessun Javadoc

L'uso di git evidenzia un uso tutto sommato corretto, forse fin troppo rigido nella divisione dei lavori:

   53  riccardo romano
    44  belletti manuel
    17  haladiy alexandr
     9  Ciosino
     9  ciosino
     1  Alexandr Haladiy
     1  Piffy@home
     1  ricky

Si nota l'assenza di commit di Fontana, il che non è bello. 


# Valutazione complessiva

In generale, pare che il gruppo nel suo complesso abbia avuto una evoluzione positiva, sviluppando una organizzazione funzionale soprattutto a fine progetto, dove sono stati trovate motivazioni ed equilibri. Tuttavia, a ben vedere, c'è ancora da lavorare. L'impegno pare distribuito in modo ineguale, cone due persone che hanno investito parecchio tempo, altre molto meno. I due subgruppi di lavoro (backend/frontend) si comunicano poco, nonostante Franciosi pare abbia fatto tentativi di comunicazione. Anche le opinioni differiscono abbastanza: per alcuni la progettazione è stata una perdita di tempo, per altri molto utile.  

Le relazioni sono state generalmente accettabili (sebbene una consegnata in ritardo) anche se nessuna è sembrata particolarmente  "ispirata" - un po' meglio quella di Belletti. 

Voto a registro.

		VOTO	Processo	Prodotto	Relazione/contributo
Haladiy		7-	5		7,8		7
Fontana		5	5		7,8		4,5
Romano		8	7		7,8		7,5
Franciosi	6,5	6		7,8		6,5
Belletti	7,5	6		7,8		7








