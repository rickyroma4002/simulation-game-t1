module com.example.provagrafico {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.provagrafico to javafx.fxml;
    exports com.example.provagrafico;
}