import java.util.ArrayList;

public class World {


    public int difficulty;  //1-ez 2-medio 3-hard
    public int i=0;
    public double countinfettatiTOT = 0;
    public double countmortiTOT = 0;
    public int startingCountry=0; //1-EU 2-Asia 3-Oceania 4-NorthAm 5-SouthAm 6-Africa
    //dichiarazione assegnazione instanziazione meglio farli qui, private per il concetto di astrazione
    public Doctor medic = new Doctor();
    public static int spreadChance = 15;
    public int counterEvent=0;

    Continente europa = new Continente(440, "europa");
    Continente sudAmerica = new Continente(420, "sudAmerica");
    Continente nordAmerica = new Continente(570, "nordAmerica");
    Continente africa = new Continente(1200, "africa");
    Continente asia = new Continente(4500, "asia");
    Continente oceania = new Continente(40, "Oceania");
    public double populationTOT = europa.population + nordAmerica.population + sudAmerica.population + africa.population + asia.population + oceania.population;


    //funzione che chiama i metodi opportuni ogni tick
    public  void  tick(){
        counterEvent++;
        europa.trigger=false;
        asia.trigger=false;
        africa.trigger=false;
        oceania.trigger=false;
        nordAmerica.trigger=false;
        sudAmerica.trigger=false;
        europa.check(medic.cure);
        if (europa.countinfettati>=europa.population/2 && (int) (Math.random()*100) <= spreadChance){
            if ((int) (Math.random()*100)<=50 && asia.hasVirus==false){
                europa.spread(asia);
            }
            if ((int) (Math.random()*100)>50 && africa.hasVirus==false){
                europa.spread(africa);
            }
        }
        nordAmerica.check(medic.cure);
        if (nordAmerica.countinfettati>=nordAmerica.population/2 && (int) (Math.random()*100) <= spreadChance && sudAmerica.hasVirus==false){
            nordAmerica.spread(sudAmerica);
        }
        sudAmerica.check(medic.cure);
        if (sudAmerica.countinfettati>=sudAmerica.population/2 && (int) (Math.random()*100) <= spreadChance){
            if ((int) (Math.random()*100)<=50 && nordAmerica.hasVirus==false){
                sudAmerica.spread(nordAmerica);
            }
            if ((int) (Math.random()*100)>50 && africa.hasVirus==false){
                sudAmerica.spread(africa);
            }
        }
        africa.check(medic.cure);
        if (africa.countinfettati>=africa.population/2 && (int) (Math.random()*100) <= spreadChance){
            if ((int) (Math.random()*100)<=50 && sudAmerica.hasVirus==false){
                africa.spread(sudAmerica);
            }
            if ((int) (Math.random()*100)>50 && europa.hasVirus==false){
                africa.spread(europa);
            }
        }
        asia.check(medic.cure);
        if (asia.countinfettati>=asia.population/2 && (int) (Math.random()*100) <= spreadChance) {
            if ((int) (Math.random() * 100) <= 50 && oceania.hasVirus == false) {
                asia.spread(oceania);
            }
            if ((int) (Math.random() * 100) > 50 && europa.hasVirus == false) {
                asia.spread(europa);
            }
        }
        oceania.check(medic.cure);
        if (oceania.countinfettati>=oceania.population/2 && (int) (Math.random()*100) <= spreadChance && asia.hasVirus==false){
            oceania.spread(asia);
        }
        countinfettatiTOT= europa.countinfettati + nordAmerica.countinfettati + sudAmerica.countinfettati + africa.countinfettati + asia.countinfettati + oceania.countinfettati;
        countmortiTOT= europa.countmorti + nordAmerica.countmorti + sudAmerica.countmorti + africa.countmorti + asia.countmorti + oceania.countmorti;
        if (countinfettatiTOT>=populationTOT/5 && medic.start==false) {medic.start=true;    System.out.println("La cura è iniziata!!");}
        if (medic.start && ((int)(Math.random()*100)<=medic.QI)){
            medic.addCure();
        }
    }

    public void startVirus(){
        if (startingCountry==1){
            europa.people.get(0).isInfected=true;
            europa.countinfettati++;
        }
        if (startingCountry==2){
            asia.people.get(0).isInfected=true;
            asia.countinfettati++;
        }
        if (startingCountry==3){
            oceania.people.get(0).isInfected=true;
            oceania.countinfettati++;
        }
        if (startingCountry==4){
            nordAmerica.people.get(0).isInfected=true;
            nordAmerica.countinfettati++;
        }
        if (startingCountry==5){
            sudAmerica.people.get(0).isInfected=true;
            sudAmerica.countinfettati++;
        }
        if (startingCountry==6){
            africa.people.get(0).isInfected=true;
            africa.countinfettati++;
        }
        countinfettatiTOT++;
    }
    public boolean bigWin(){
        if (countmortiTOT==populationTOT){
            return true;
        }
        else{
            return false;
        }
    }
    public boolean bigLost(){
        if (medic.cure==20){
            return true;
        }
        else if (countinfettatiTOT==0 && countmortiTOT!=populationTOT){
            return true;
        }
        else{
            return false;
        }
    }

    public World() {}

}
