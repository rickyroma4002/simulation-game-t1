import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.sql.SQLOutput;

//TODO  IMPORTANTE finire completamente game entro 18/01 e se possibile dare delle funzionalità a opzioni MA GAME RIMANE
//TODO  LA PRIORITA'
public class HelloController {
    @FXML
    private javafx.scene.control.Button closeButton;
    public Stage stage;
    public Scene scene;
    public Parent root;
    World game = new World();
    public Timeline t1 = new Timeline(new KeyFrame(Duration.millis(700), e -> run(scene)));
    public void play(ActionEvent event) throws IOException, InterruptedException {
        switchToSceneGame(event);
        game.startVirus();
        t1.setCycleCount(Timeline.INDEFINITE);
        stage.show();
        if (game.difficulty==1)game.medic.QI=10;
        if (game.difficulty==2)game.medic.QI=15;
        if (game.difficulty==3)game.medic.QI=25;
        t1.play();
        game.counterEvent=0;
        game.europa.virus.SetInfectivity(50);
        game.europa.virus.SetMortality(0);
        game.spreadChance=15;
    }
    public void run(Scene scene){
        game.tick();
        if (game.europa.trigger){
            TextField spreaded = (TextField) scene.lookup("#spreaded");
            game.counterEvent=0;
            spreaded.setVisible(true);
            spreaded.setText("E'stato registrato il primo caso di virus in EUROPA");
        }
        if (game.asia.trigger){
            TextField spreaded = (TextField) scene.lookup("#spreaded");
            game.counterEvent=0;
            spreaded.setVisible(true);
            spreaded.setText("E'stato registrato il primo caso di virus in ASIA");
        }
        if (game.africa.trigger){
            TextField spreaded = (TextField) scene.lookup("#spreaded");
            game.counterEvent=0;
            spreaded.setVisible(true);
            spreaded.setText("E'stato registrato il primo caso di virus in AFRICA");
        }
        if (game.oceania.trigger){
            TextField spreaded = (TextField) scene.lookup("#spreaded");
            game.counterEvent=0;
            spreaded.setVisible(true);
            spreaded.setText("E'stato registrato il primo caso di virus in OCEANIA");
        }
        if (game.nordAmerica.trigger){
            TextField spreaded = (TextField) scene.lookup("#spreaded");
            game.counterEvent=0;
            spreaded.setVisible(true);
            spreaded.setText("E'stato registrato il primo caso di virus in NORD AMERICA");
        }
        if (game.sudAmerica.trigger){
            TextField spreaded = (TextField) scene.lookup("#spreaded");
            game.counterEvent=0;
            spreaded.setVisible(true);
            spreaded.setText("E'stato registrato il primo caso di virus in SUD AMERICA");
        }

        if(game.counterEvent>=5){
            TextField spreaded = (TextField) scene.lookup("#spreaded");
            spreaded.setVisible(false);
        }
        System.out.println("MORTI: "+ game.countmortiTOT + "\n INFETTI: "+ game.countinfettatiTOT);
        ProgressBar infetti = (ProgressBar) scene.lookup("#infetti");
        ProgressBar morti = (ProgressBar) scene.lookup("#morti");

        ProgressBar infettiEU = (ProgressBar) scene.lookup("#infettiEU");
        ProgressBar mortiEU = (ProgressBar) scene.lookup("#mortiEU");

        ProgressBar infettiAS = (ProgressBar) scene.lookup("#infettiAS");
        ProgressBar mortiAS = (ProgressBar) scene.lookup("#mortiAS");

        ProgressBar infettiOC = (ProgressBar) scene.lookup("#infettiOC");
        ProgressBar mortiOC = (ProgressBar) scene.lookup("#mortiOC");

        ProgressBar infettiNA = (ProgressBar) scene.lookup("#infettiNA");
        ProgressBar mortiNA = (ProgressBar) scene.lookup("#mortiNA");

        ProgressBar infettiAF = (ProgressBar) scene.lookup("#infettiAF");
        ProgressBar mortiAF = (ProgressBar) scene.lookup("#mortiAF");

        ProgressBar infettiSA = (ProgressBar) scene.lookup("#infettiSA");
        ProgressBar mortiSA = (ProgressBar) scene.lookup("#mortiSA");

        ProgressBar cura = (ProgressBar) scene.lookup("#cura");
        TextField upInf = (TextField) scene.lookup("#upInf");
        TextField upMor = (TextField) scene.lookup("#upMor");
        TextField upSpr = (TextField) scene.lookup("#upSpr") ;
        upInf.setText("" + game.europa.virus.infettivity);
        upMor.setText("" + game.europa.virus.mortality);
        upSpr.setText(""+ game.spreadChance);
            infetti.setProgress(game.countinfettatiTOT/7170); infetti.setStyle("-fx-accent: red;");
            morti.setProgress(game.countmortiTOT/7170); morti.setStyle("-fx-accent: black;");

            infettiEU.setProgress(game.europa.countinfettati/440); infettiEU.setStyle("-fx-accent: red;");
            mortiEU.setProgress(game.europa.countmorti/440); mortiEU.setStyle("-fx-accent: black;");

            infettiAS.setProgress(game.asia.countinfettati/4500); infettiAS.setStyle("-fx-accent: red;");
            mortiAS.setProgress(game.asia.countmorti/4500); mortiAS.setStyle("-fx-accent: black;");

            infettiAF.setProgress(game.africa.countinfettati/1200); infettiAF.setStyle("-fx-accent: red;");
            mortiAF.setProgress(game.africa.countmorti/1200); mortiAF.setStyle("-fx-accent: black;");

            infettiOC.setProgress(game.oceania.countinfettati/40); infettiOC.setStyle("-fx-accent: red;");
            mortiOC.setProgress(game.oceania.countmorti/40); mortiOC.setStyle("-fx-accent: black;");

            infettiNA.setProgress(game.nordAmerica.countinfettati/570); infettiNA.setStyle("-fx-accent: red;");
            mortiNA.setProgress(game.nordAmerica.countmorti/570); mortiNA.setStyle("-fx-accent: black;");

            infettiSA.setProgress(game.sudAmerica.countinfettati/420); infettiSA.setStyle("-fx-accent: red;");
            mortiSA.setProgress(game.sudAmerica.countmorti/420); mortiSA.setStyle("-fx-accent: black;");

            cura.setProgress(game.medic.cure/20);   cura.setStyle("-fx-accent: blue;");
        if (game.bigWin()){
            t1.stop();
            TextField virusWin = (TextField) scene.lookup("#virusWin");
            virusWin.setVisible(true);
        }
        if (game.bigLost()){
            TextField virusLost = (TextField) scene.lookup("#virusLost");
            virusLost.setVisible(true);
            t1.stop();
        }

    }
    public void closeStage(ActionEvent actionEvent) {
        Node source = (Node) actionEvent.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }
    public void switchToSceneMenu(ActionEvent event) throws IOException {
        t1.stop();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("menu.fxml"));
        Parent root = fxmlLoader.load();
        stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        this.scene = new Scene(root, 801, 600);
        stage.setScene(scene);
        stage.show();
    }
    public void switchToSceneOpzioni(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("opzioni.fxml"));
        Parent root = fxmlLoader.load();
        stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root, 800, 600);
        stage.setScene(scene);
        stage.show();
    }
    public void switchToSceneGame(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("game.fxml"));
        Parent root = fxmlLoader.load();
        stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root, 800, 600);
        stage.setScene(scene);
        stage.show();
    }

    public void selectEU(ActionEvent event) throws IOException{
        game.startingCountry=1;
        Node source = (Node) event.getSource();
        stage = (Stage) source.getScene().getWindow();
        TextField selected = (TextField) stage.getScene().lookup("#selected");
        ProgressBar infettiEU = (ProgressBar) stage.getScene().lookup("#infettiEU");
        ProgressBar mortiEU = (ProgressBar) stage.getScene().lookup("#mortiEU");
        ProgressBar infettiAS = (ProgressBar) stage.getScene().lookup("#infettiAS");
        ProgressBar mortiAS = (ProgressBar) stage.getScene().lookup("#mortiAS");
        ProgressBar infettiOC = (ProgressBar) stage.getScene().lookup("#infettiOC");
        ProgressBar mortiOC = (ProgressBar) stage.getScene().lookup("#mortiOC");
        ProgressBar infettiNA = (ProgressBar) stage.getScene().lookup("#infettiNA");
        ProgressBar mortiNA = (ProgressBar) stage.getScene().lookup("#mortiNA");
        ProgressBar infettiAF = (ProgressBar) stage.getScene().lookup("#infettiAF");
        ProgressBar mortiAF = (ProgressBar) stage.getScene().lookup("#mortiAF");
        ProgressBar infettiSA = (ProgressBar) stage.getScene().lookup("#infettiSA");
        ProgressBar mortiSA = (ProgressBar) stage.getScene().lookup("#mortiSA");
        infettiEU.setVisible(true); mortiEU.setVisible(true);
        infettiAS.setVisible(false); mortiAS.setVisible(false);
        infettiAF.setVisible(false); mortiAF.setVisible(false);
        infettiOC.setVisible(false); mortiOC.setVisible(false);
        infettiNA.setVisible(false); mortiNA.setVisible(false);
        infettiSA.setVisible(false); mortiSA.setVisible(false);
        selected.setText("Selected: Europe");
    }
    public void selectAF(ActionEvent event) throws IOException{
        game.startingCountry=6;
        Node source = (Node) event.getSource();
        stage = (Stage) source.getScene().getWindow();
        TextField selected = (TextField) stage.getScene().lookup("#selected");
        ProgressBar infettiEU = (ProgressBar) stage.getScene().lookup("#infettiEU");
        ProgressBar mortiEU = (ProgressBar) stage.getScene().lookup("#mortiEU");
        ProgressBar infettiAS = (ProgressBar) stage.getScene().lookup("#infettiAS");
        ProgressBar mortiAS = (ProgressBar) stage.getScene().lookup("#mortiAS");
        ProgressBar infettiOC = (ProgressBar) stage.getScene().lookup("#infettiOC");
        ProgressBar mortiOC = (ProgressBar) stage.getScene().lookup("#mortiOC");
        ProgressBar infettiNA = (ProgressBar) stage.getScene().lookup("#infettiNA");
        ProgressBar mortiNA = (ProgressBar) stage.getScene().lookup("#mortiNA");
        ProgressBar infettiAF = (ProgressBar) stage.getScene().lookup("#infettiAF");
        ProgressBar mortiAF = (ProgressBar) stage.getScene().lookup("#mortiAF");
        ProgressBar infettiSA = (ProgressBar) stage.getScene().lookup("#infettiSA");
        ProgressBar mortiSA = (ProgressBar) stage.getScene().lookup("#mortiSA");
        infettiEU.setVisible(false); mortiEU.setVisible(false);
        infettiAS.setVisible(false); mortiAS.setVisible(false);
        infettiAF.setVisible(true); mortiAF.setVisible(true);
        infettiOC.setVisible(false); mortiOC.setVisible(false);
        infettiNA.setVisible(false); mortiNA.setVisible(false);
        infettiSA.setVisible(false); mortiSA.setVisible(false);
        selected.setText("Selected: Africa");
    }
    public void selectAS(ActionEvent event) throws IOException{
        game.startingCountry=2;
        Node source = (Node) event.getSource();
        stage = (Stage) source.getScene().getWindow();
        TextField selected = (TextField) stage.getScene().lookup("#selected");
        ProgressBar infettiEU = (ProgressBar) stage.getScene().lookup("#infettiEU");
        ProgressBar mortiEU = (ProgressBar) stage.getScene().lookup("#mortiEU");
        ProgressBar infettiAS = (ProgressBar) stage.getScene().lookup("#infettiAS");
        ProgressBar mortiAS = (ProgressBar) stage.getScene().lookup("#mortiAS");
        ProgressBar infettiOC = (ProgressBar) stage.getScene().lookup("#infettiOC");
        ProgressBar mortiOC = (ProgressBar) stage.getScene().lookup("#mortiOC");
        ProgressBar infettiNA = (ProgressBar) stage.getScene().lookup("#infettiNA");
        ProgressBar mortiNA = (ProgressBar) stage.getScene().lookup("#mortiNA");
        ProgressBar infettiAF = (ProgressBar) stage.getScene().lookup("#infettiAF");
        ProgressBar mortiAF = (ProgressBar) stage.getScene().lookup("#mortiAF");
        ProgressBar infettiSA = (ProgressBar) stage.getScene().lookup("#infettiSA");
        ProgressBar mortiSA = (ProgressBar) stage.getScene().lookup("#mortiSA");
        infettiEU.setVisible(false); mortiEU.setVisible(false);
        infettiAS.setVisible(true); mortiAS.setVisible(true);
        infettiAF.setVisible(false); mortiAF.setVisible(false);
        infettiOC.setVisible(false); mortiOC.setVisible(false);
        infettiNA.setVisible(false); mortiNA.setVisible(false);
        infettiSA.setVisible(false); mortiSA.setVisible(false);
        selected.setText("Selected: Asia");
    }
    public void selectOC(ActionEvent event) throws IOException{
        game.startingCountry=3;
        Node source = (Node) event.getSource();
        stage = (Stage) source.getScene().getWindow();
        TextField selected = (TextField) stage.getScene().lookup("#selected");
        ProgressBar infettiEU = (ProgressBar) stage.getScene().lookup("#infettiEU");
        ProgressBar mortiEU = (ProgressBar) stage.getScene().lookup("#mortiEU");
        ProgressBar infettiAS = (ProgressBar) stage.getScene().lookup("#infettiAS");
        ProgressBar mortiAS = (ProgressBar) stage.getScene().lookup("#mortiAS");
        ProgressBar infettiOC = (ProgressBar) stage.getScene().lookup("#infettiOC");
        ProgressBar mortiOC = (ProgressBar) stage.getScene().lookup("#mortiOC");
        ProgressBar infettiNA = (ProgressBar) stage.getScene().lookup("#infettiNA");
        ProgressBar mortiNA = (ProgressBar) stage.getScene().lookup("#mortiNA");
        ProgressBar infettiAF = (ProgressBar) stage.getScene().lookup("#infettiAF");
        ProgressBar mortiAF = (ProgressBar) stage.getScene().lookup("#mortiAF");
        ProgressBar infettiSA = (ProgressBar) stage.getScene().lookup("#infettiSA");
        ProgressBar mortiSA = (ProgressBar) stage.getScene().lookup("#mortiSA");
        infettiEU.setVisible(false); mortiEU.setVisible(false);
        infettiAS.setVisible(false); mortiAS.setVisible(false);
        infettiAF.setVisible(false); mortiAF.setVisible(false);
        infettiOC.setVisible(true); mortiOC.setVisible(true);
        infettiNA.setVisible(false); mortiNA.setVisible(false);
        infettiSA.setVisible(false); mortiSA.setVisible(false);
        selected.setText("Selected: Oceania");
    }
    public void selectNA(ActionEvent event) throws IOException{
        game.startingCountry=4;
        Node source = (Node) event.getSource();
        stage = (Stage) source.getScene().getWindow();
        TextField selected = (TextField) stage.getScene().lookup("#selected");
        ProgressBar infettiEU = (ProgressBar) stage.getScene().lookup("#infettiEU");
        ProgressBar mortiEU = (ProgressBar) stage.getScene().lookup("#mortiEU");
        ProgressBar infettiAS = (ProgressBar) stage.getScene().lookup("#infettiAS");
        ProgressBar mortiAS = (ProgressBar) stage.getScene().lookup("#mortiAS");
        ProgressBar infettiOC = (ProgressBar) stage.getScene().lookup("#infettiOC");
        ProgressBar mortiOC = (ProgressBar) stage.getScene().lookup("#mortiOC");
        ProgressBar infettiNA = (ProgressBar) stage.getScene().lookup("#infettiNA");
        ProgressBar mortiNA = (ProgressBar) stage.getScene().lookup("#mortiNA");
        ProgressBar infettiAF = (ProgressBar) stage.getScene().lookup("#infettiAF");
        ProgressBar mortiAF = (ProgressBar) stage.getScene().lookup("#mortiAF");
        ProgressBar infettiSA = (ProgressBar) stage.getScene().lookup("#infettiSA");
        ProgressBar mortiSA = (ProgressBar) stage.getScene().lookup("#mortiSA");
        infettiEU.setVisible(false); mortiEU.setVisible(false);
        infettiAS.setVisible(false); mortiAS.setVisible(false);
        infettiAF.setVisible(false); mortiAF.setVisible(false);
        infettiOC.setVisible(false); mortiOC.setVisible(false);
        infettiNA.setVisible(true); mortiNA.setVisible(true);
        infettiSA.setVisible(false); mortiSA.setVisible(false);
        selected.setText("Selected: North America");
    }
    public void selectSA(ActionEvent event) throws IOException{
        game.startingCountry=5;
        Node source = (Node) event.getSource();
        stage = (Stage) source.getScene().getWindow();
        TextField selected = (TextField) stage.getScene().lookup("#selected");ProgressBar infettiEU = (ProgressBar) stage.getScene().lookup("#infettiEU");
        ProgressBar mortiEU = (ProgressBar) stage.getScene().lookup("#mortiEU");
        ProgressBar infettiAS = (ProgressBar) stage.getScene().lookup("#infettiAS");
        ProgressBar mortiAS = (ProgressBar) stage.getScene().lookup("#mortiAS");
        ProgressBar infettiOC = (ProgressBar) stage.getScene().lookup("#infettiOC");
        ProgressBar mortiOC = (ProgressBar) stage.getScene().lookup("#mortiOC");
        ProgressBar infettiNA = (ProgressBar) stage.getScene().lookup("#infettiNA");
        ProgressBar mortiNA = (ProgressBar) stage.getScene().lookup("#mortiNA");
        ProgressBar infettiAF = (ProgressBar) stage.getScene().lookup("#infettiAF");
        ProgressBar mortiAF = (ProgressBar) stage.getScene().lookup("#mortiAF");
        ProgressBar infettiSA = (ProgressBar) stage.getScene().lookup("#infettiSA");
        ProgressBar mortiSA = (ProgressBar) stage.getScene().lookup("#mortiSA");
        infettiEU.setVisible(false); mortiEU.setVisible(false);
        infettiAS.setVisible(false); mortiAS.setVisible(false);
        infettiAF.setVisible(false); mortiAF.setVisible(false);
        infettiOC.setVisible(false); mortiOC.setVisible(false);
        infettiNA.setVisible(false); mortiNA.setVisible(false);
        infettiSA.setVisible(true); mortiSA.setVisible(true);
        selected.setText("Selected: South America");
    }
    public void selectWR(ActionEvent event) throws IOException{
        game.startingCountry=0;
        Node source = (Node) event.getSource();
        stage = (Stage) source.getScene().getWindow();
        TextField selected = (TextField) stage.getScene().lookup("#selected");
        ProgressBar infettiEU = (ProgressBar) stage.getScene().lookup("#infettiEU");
        ProgressBar mortiEU = (ProgressBar) stage.getScene().lookup("#mortiEU");
        ProgressBar infettiAS = (ProgressBar) stage.getScene().lookup("#infettiAS");
        ProgressBar mortiAS = (ProgressBar) stage.getScene().lookup("#mortiAS");
        ProgressBar infettiOC = (ProgressBar) stage.getScene().lookup("#infettiOC");
        ProgressBar mortiOC = (ProgressBar) stage.getScene().lookup("#mortiOC");
        ProgressBar infettiNA = (ProgressBar) stage.getScene().lookup("#infettiNA");
        ProgressBar mortiNA = (ProgressBar) stage.getScene().lookup("#mortiNA");
        ProgressBar infettiAF = (ProgressBar) stage.getScene().lookup("#infettiAF");
        ProgressBar mortiAF = (ProgressBar) stage.getScene().lookup("#mortiAF");
        ProgressBar infettiSA = (ProgressBar) stage.getScene().lookup("#infettiSA");
        ProgressBar mortiSA = (ProgressBar) stage.getScene().lookup("#mortiSA");
        infettiEU.setVisible(false); mortiEU.setVisible(false);
        infettiAS.setVisible(false); mortiAS.setVisible(false);
        infettiAF.setVisible(false); mortiAF.setVisible(false);
        infettiOC.setVisible(false); mortiOC.setVisible(false);
        infettiNA.setVisible(false); mortiNA.setVisible(false);
        infettiSA.setVisible(false); mortiSA.setVisible(false);
        selected.setText("Selected: World");
    }

    public void difficultyEZ(ActionEvent event) throws IOException{
        game.difficulty=1;
    }
    public void difficultyMED(ActionEvent event) throws IOException{
        game.difficulty=2;
    }
    public void difficultyHAR(ActionEvent event) throws IOException{
        game.difficulty=3;
    }

    public void upInfettivity(ActionEvent event) throws  IOException{
        game.europa.virus.infettivity+=1;
    }
    public void upMortality(ActionEvent event) throws IOException{
        game.europa.virus.mortality+=1;
    }
    public void upSpreadChance(ActionEvent event) throws IOException{
        game.spreadChance++;
    }
}