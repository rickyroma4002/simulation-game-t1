import java.util.ArrayList;

public class Continente {
    private String name;
    public int population = 0;
    public ArrayList<Human> people = new ArrayList<Human>(10000);
    public boolean hasVirus=false;
    public Virus virus = new Virus();
    public double countinfettati = 0;
    public double countmorti=0;
    public boolean trigger=false;

    public Continente(int population, String name){
        this.population=population;
        this.name=name;
        for(int i=0; i<this.population; i++){
            people.add(new Human());
        }
    }

    public void check(double cure){
        if (countinfettati>0)hasVirus=true;
        for (int i=0; i< people.size(); i++){
            if (people.get(i).isInfected==true && (int) (Math.random()*100)<=virus.infettivity){
                int bersaglio=(int) (Math.random()*population);
                if (people.get(bersaglio).isInfected==false && people.get(bersaglio).isDead==false){
                    people.get(i).infect(people.get(bersaglio));
                    countinfettati++;
                }
            }
            if (people.get(i).isDead==false && (Math.random()*100)<=virus.mortality && people.get(i).isInfected==true){
                people.get(i).die();
                countinfettati--;
                countmorti++;
            }
        }
    }
    public void spread(Continente continente){
        continente.people.get(0).isInfected=true;
        continente.hasVirus=true;
        System.out.println(continente.toString()+ " è stato infettato");
        countinfettati++;
        continente.trigger=true;
    }
    public String toString(){
        return name;
    }
}
